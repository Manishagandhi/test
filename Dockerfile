FROM ubuntu
MAINTAINER user@gmail.com
RUN apt-get update
RUN apt-get install -y openjdk-8-jdk
ADD target/micro-service-0.0.1-SNAPSHOT.jar /var/local/
CMD java -jar /var/local/micro-service-0.0.1-SNAPSHOT.jar
EXPOSE 8080
