package com;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@RequestMapping(value="/hello", produces=MediaType.APPLICATION_JSON_VALUE)
	public User hello(HttpServletRequest request, User user) {
		return user;
	}

}
